import './App.css'
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';
import {HashRouter ,Route, Routes} from 'react-router-dom';
import Home from './components/pages/home/Home';
import ECGO3 from './components/pages/ecgo3/ECGO3';
import ECGO5 from './components/pages/ecgo5/ECGO5';
import Dealer from './components/pages/dealer/Dealer';
import Partner from './components/pages/partner/Partner';
import Contact from './components/pages/contact/Contact';
import About from './components/pages/aboutus/About';
import FAQ from './components/pages/faq/FAQ';

function App() {
  return (
    <HashRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/ecgo-3" element={<ECGO3 />}/>
        <Route path="/ecgo-5" element={<ECGO5 />}/>
        <Route path="/dealer" element={<Dealer />}/>
        <Route path="/partner" element={<Partner />}/>
        <Route path="/contact" element={<Contact />}/>
        <Route path="/aboutus" element={<About />}/>
        <Route path="/faq" element={<FAQ />}/>
      </Routes>
      <Footer/>
    </HashRouter>
    )
}

export default App;
