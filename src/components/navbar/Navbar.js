import React from 'react'
import './navbar.css'
import { Link } from 'react-router-dom'
import {FaBars, FaTimes} from 'react-icons/fa'
import { useState } from 'react'
import NavDropdown from 'react-bootstrap/NavDropdown';
import 'bootstrap/dist/css/bootstrap.min.css';

const Navbar = () => {
    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);


    return (
    <div className='navbar'>
        <div className='navbar-container container'>
            <div className='container-left'>
                <Link to='/' className='navbar-logo'>
                    <img src={require('../../assets/images/LogoWhite.png')} className='navlogo' />
                </Link>
            </div>
                <div className='menu-icon' onClick={handleClick}>
                    {click ? <FaTimes/> : <FaBars/>}
                </div>
                    <div className={click ? 'container-right expanded':'container-right'}>       
                            <NavDropdown title='Products' className='navbar-link'>
                                <NavDropdown.Item className='dropdown-item'>
                                    <Link to="/ecgo-3">ECGO 3</Link>
                                </NavDropdown.Item>
                                <NavDropdown.Item className='dropdown-item'>
                                    <Link to="/ecgo-5">ECGO 5</Link>
                                </NavDropdown.Item>
                            </NavDropdown>
                            <Link to='/dealer' className='navbar-link'>
                                Dealer
                            </Link> 
                            <Link to='/partner' className='navbar-link'>
                                Partner
                            </Link> 
                            <Link to='/contact' className='navbar-link'>
                                Contact
                            </Link> 
                            <Link to='/aboutus' className='navbar-link'>
                                About Us
                            </Link> 
                            <Link to='/faq' className='navbar-link'>
                                FAQ
                            </Link>            
                    </div>
            
        </div>
    </div>
  )
}

export default Navbar