import React from 'react'
import './footer.css'
import {BsFacebook, BsInstagram, BsYoutube} from 'react-icons/bs'
import {AiFillInstagram, AiFillFacebook } from 'react-icons/ai'
import {ImFacebook2 } from 'react-icons/im'
import { Link } from 'react-router-dom'

const Footer = () => { 
  return (
    <div className='mainFooter'>
      <div className='footerContainer'>
        <div className='upper-container'>
          <div className='left-upper-container'>
            <a className='sosmed' href='https://www.instagram.com/ecgobikeofficial.id/'>Instagram</a>
            <a className='sosmed' href='https://www.facebook.com/pages/category/Brand/Ecgo-Bike-Indonesia-106709374006934/'>Facebook</a>
            <a className='sosmed' href='https://www.youtube.com/channel/UCN85tXjLVEyaZ6czq9I38zA/featured'>Youtube</a>
            <a className='sosmed' href='/'>Home</a>
            <a className='sosmed' href='/ECGO3'>ECGO 3</a>
            <a className='sosmed' href='/ECGO5'>ECGO 5</a>
            <a className='sosmed' href='/Dealer'>Dealer</a>
            <a className='sosmed' href='/partner'>Partner</a>
            <a className='sosmed' href='/aboutus'>About Us</a>
            <a className='sosmed' href='/contact'>Contact Us</a>
            
          </div>
          <div className='right-upper-container'>
              <a className='wa-link' href='https://wa.me/+6282122339126'>
              <img className='wa-footer' src={require('../../assets/images/wafooter.png')}/>
              </a>
          </div>
        </div>
       
        <div className='text-container'>
          <a className='copyright'>© 2019 Copyright PT Green City Traffic</a>
        </div>
      </div>
    </div>
  )
}

export default Footer