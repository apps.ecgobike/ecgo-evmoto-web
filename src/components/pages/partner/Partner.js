import axios from 'axios';
import { useState, React, useEffect } from 'react'
import { DynamicTitle } from '../../utils/DynamicTitle'
import './partner.css'


const Partner = () => {
  DynamicTitle('Partner | ECGO');

  const [formKey, setFormKey] = useState(0);
  const [formData, setFormData] = useState({
    name:'', 
    companyName:'',
    telp:'',
    email:'',
    address:'',
    city:''
  })

  function handleChange(e) {
    let data = {...formData}
    data[e.target.name]= e.target.value; 
    setFormData(data);
  }

  function handleSubmit(e) {
    e.preventDefault();
    let newData= {name: formData.name, companyName: formData.companyName, telp: formData.telp, email: formData.email, address: formData.address, city:formData.city};
    axios.post('https://ecgobikeid.com/dealer/register', newData).then(res => {
      console.log(res);
    })
    alert('Terimakasih telah melakukan pendaftaran menjadi mitra ECGO Bike')
    setFormKey(formKey+1);
  }
  
  return (
    <div className='partner'>
      <div className='partner-container'>
        <div className='partner-container-left'>
          <h1 className='partner-title'>Bergabung bersama kami menjadi mitra ECGO BIKE!</h1>
        </div>
        <div className='partner-container-right'>
          <form className='form-partner' key={formKey}>
            <div className='form-group-partner'>
              <label className='nama'>Nama  </label>
              <input 
              className='input-nama'  
              required
              type='text'
              name='name'
              value={formData.name}
              onChange={handleChange}
              />
            </div>
            <div className='form-group-partner'>
              <label className='nama'>Nama Perusahaan  </label>
              <input 
              className='input-perusahaan' 
              required
              type='text'
              name='companyName'
              value={formData.companyName}
              onChange={handleChange} 
              />
            </div>
            <div className='form-group-partner'>
              <label className='nama'>No HP / Telp  </label>
              <input 
              className='input-telp' 
              required
              type='string'
              name='telp'
              value={formData.telp}
              onChange={handleChange}
              />
            </div>
            <div className='form-group-partner'>
              <label className='nama'>E-Mail  </label>
              <input 
              className='input-email' 
              required
              type='email'
              name='email'
              value={formData.email}
              onChange={handleChange}
              />
            </div>
            <div className='form-group-partner'>
              <label className='nama'>Alamat  </label>
              <input 
              className='input-alamat' 
              required
              type='text'
              name='address'
              value={formData.address}
              onChange={handleChange}
              />
            </div>
            <div className='form-group-partner'>
              <label className='nama'>Kota  </label>
              <input 
              className='input-kota' 
              required
              type='text'
              name='city'
              value={formData.city}
              onChange={handleChange}
              />
            </div>
              <button 
              className='btn-partner'
              disabled={!formData.name}
              onClick={handleSubmit}>
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Partner