import React from 'react'
import './home.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel from 'react-bootstrap/Carousel';
import { DynamicTitle } from '../../utils/DynamicTitle';

const Home = () => {
  DynamicTitle('Home | ECGO');
  
  return (
    <div className='home'>
      <div className='carousel-container'>
        <Carousel fade>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require('../../../assets/images/Home1.jpg')}
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require('../../../assets/images/Home2.jpg')}
              alt="Second slide"
            />
          </Carousel.Item>
        </Carousel>
      </div>
      <div className='app-container'>
        <div className='app-container-left'>
          <img className='app-gif' src={require('../../../assets/images/app.gif')}/>
        </div>
        <div className='app-container-right2'>
          <a href='#'><img className='appstore-logo' src={require('../../../assets/images/pngegg.png')}/></a> 
          <a href='https://play.google.com/store/apps/details?id=com.ecgo.moto
          '><img className='appstore-logo' src={require('../../../assets/images/pngegg2.png')}/></a> 
        </div>
      <div className='app-container-right'>
        <h1>Terhubung Dengan Aplikasi ECGO</h1>
        <li>Lokasi Kendaraan</li>
        <p>Membantu anda menghetahui posisi kendaraan anda setiap saat</p>
        <li>Informasi Kendaraan & Baterai</li>
        <p>Membantu anda mengetahui dan memantau kondisi baterai yang anda miliki</p>
      </div>
      </div>
      <div className='fitur-container'>
       <img className='fitur-img' src={require('../../../assets/images/fitur.jpeg')}/>
      </div>
      <div className='sub-container'>
        <a className='text-sub'>Subscribe pada newsletter kami untuk mendapatkan penawaran menarik</a>
        <input className='sub-input' type='text' placeholder='  Daftarkan email anda disini'/>
        <button className='sub-button'>Subscribe</button>
      </div>
    </div>
  )
}

export default Home