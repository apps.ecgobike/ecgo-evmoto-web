import {React, useState} from 'react'
import axios from 'axios'
import './contact.css'
import {GrLocation,GrPhone,GrMailOption} from 'react-icons/gr'
import { Map,Wrapper, Status } from "@googlemaps/react-wrapper";
import { DynamicTitle } from '../../utils/DynamicTitle';

const Contact = () => {
  DynamicTitle('Contact Us | ECGO');
  
  const [formKey, setFormKey] = useState(0);
  const [formData, setFormData] = useState({
    name:'', 
    email:'',
    pesan:''
  })

  function handleChange(e) {
    let data = {...formData}
    data[e.target.name]= e.target.value; 
    setFormData(data);
  }

  function handleSubmit(e) {
    e.preventDefault();
    let newData= {name: formData.name, email: formData.email,pesan:formData.pesan};
    axios.post('https://ecgobikeid.com/customer/contact', newData).then(res => {
      console.log(res);
    })
    alert('Terimakasih telah memberikan pesan atau masukan untuk kami. Kami akan membalas secepatnya melalui email')
    setFormKey(formKey+1);
  }

  return (
    <div className='contact'>
        <div className='img-container'>
          <img src={require('../../../assets/images/contactus/heldesk2.jpg')} className='contactbg' />
        </div>
        <div className='contact-content'>
        <div className='contact-container'>
          <div className='contact-text'>
            <h1 className='contact-title'>Contact Us</h1>
            <p className='contact-text1'>We Love To Hear Your Feedback</p>
            <p className='contact-text1'>Masukkan Nama dan Email, kemudian tinggalkan pesan untuk kami.</p>
          </div>
          <div className='contact-form-container'>
          <form className='form'>
            <div className='form-group'>
              <label className='nama'>Nama  </label>
              <input 
              className='input-nama'
              type='text'
              name='name'
              onChange={handleChange}
              />
            </div>
            <div className='form-group'>
              <label className='nama'>E-Mail  </label>
              <input 
              className='input-nama'
              type='text'
              name='email'
              onChange={handleChange}
              />
            </div>
            <div className='form-group'>
              <label className='nama'>Pesan  </label>
              <input 
              className='input-pesan'
              type='text'
              name='pesan'
              onChange={handleChange}
              />
            </div>
           
            <button className='btn' disabled={!formData.pesan} onClick={handleSubmit}>
              Submit
            </button>
          </form>
          </div>
        </div>
        </div>
        <div className='detail-container'>
          <div className='detail-row'>
            
            <div className='detail-address'>
              
              <h1>Address</h1>
              <a>Springhill Office Tower unit 15E</a>
              <a>Jl. Benyamin Sueb Ruas D7 Blok D6</a>
              <a>Pademangan Timur, Pademangan</a>
              <a>Jakarta Utara, 14410</a>
            </div>
            <div className='detail-phone'>
              <h1 className='phone'>Phone</h1>
              <a>021 - 26058818</a>
            </div>
            <div className='detail-email'>
              <h1 className='email'>Email</h1>
              <a>admin@ecgobike.co.id</a>
              <p></p>
            </div>
          </div>
        </div>
        <div className='maps-container'>
        <div className="mapouter">
          <div className="map_canvas">
            <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=ECGO%20head%20office&t=&z=13&ie=UTF8&iwloc=&output=embed" frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0"></iframe>
          </div>
          </div>
        </div>
    </div>
  )
}

export default Contact