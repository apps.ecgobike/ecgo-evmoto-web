import React from 'react'
import './faq.css'

const FAQ = () => {
  return (
    <div className='faq'>
        <h1>Frequently Asked Question</h1>
        <div className='faq-container'>
        <ul>
            <li>Berapa lama pengisian baterai hingga penuh ?</li>
            <li>Dengan pengisi daya 5A membutuhkan waktu 5-6 jam, dan dengan fast charging 9A membutuhkan waktu 2,5 - 3 jam</li>
            <li>Berapa lama pengisian baterai hingga penuh ?</li>
            <li>Kecepatan maksimal motor dapat menyentuh 75 Km/Jam dengan beban normal</li>
            <li>Apakah ada beban maksimum?</li>
            <li>ECGO dapat mengangkut beban hingga 150 Kg s/d 200 Kg (termasuk pengendara dan penumpang)</li>
            <li>Apakah tahan terhadap air?</li>
            <li>ECGO tahan terhadap air, bahkan dalam kondisi air menggenang setinggi 30 cm ECGO masih dapat melaju dengan normal</li>
            <li>Adakah garansi yang diberikan?</li>
            <li>Ya</li>
            <li>Apa saja yang menjadi bagian dari garansi?</li>
            <li>ECGO memberikan garansi atas baterai, motor dan DCU, rangka, ban, dan komponen lainnya</li>
            <li>Dimana lokasi service ECGO?</li>
            <li>ECGO berkomitmen memiliki jaringan service terluas di Indonesia, dan kini dapat dijumpai di bengkel-bengkel terdekat yang bertanda ECGO Official Partner di seluruh wilayah Indonesia</li>
        </ul>
        </div>
    </div>
  )
}

export default FAQ