import React from 'react'
import { DynamicTitle } from '../../utils/DynamicTitle'
import './about.css'

const About = () => {
  DynamicTitle('About Us | ECGO');

  return (
    <div className='aboutus'>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0001.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0003.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0004.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0005.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0006.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0007.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0008.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0009.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0010.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0011.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0012.jpg')}/>
      <img className='about-img' src={require('../../../assets/images/aboutus/ECGO Company Profile Draft_page-0013.jpg')}/>
    </div>
  )
}

export default About