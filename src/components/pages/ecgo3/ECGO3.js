import React, { useState } from 'react'
import { DynamicTitle } from '../../utils/DynamicTitle'
import './ecgo3.css'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import brosur from '../../../assets/brosur3.pdf'

const ECGO3 = () => {
  DynamicTitle('ECGO 3 | ECGO');

  const putih = require('../../../assets/images/ecgo3/putih - 3.png');
  const hitam = require('../../../assets/images/ecgo3/hitam - 3.png');
  const merah = require('../../../assets/images/ecgo3/merah - 3.png');
  const biru = require('../../../assets/images/ecgo3/biru - 3.png');
  const ecgo3 = {hitam, putih, merah, biru};

  const [selected, setSelected] = useState(ecgo3.putih)

  return (
    <div className='ecgo3'>
      <div className='ecgo3-container'>
       <img id='ecgo3-img' className='ecgo3-img' src={selected}/>
      </div>
      <div className='color-container'>
          <Button variant="primary" className='btn-putih' onClick={()=> setSelected(ecgo3.putih)}>Pearl White</Button>
          <Button variant="primary" className='btn-hitam' onClick={()=> setSelected(ecgo3.hitam)}>Iron Black</Button>
          <Button variant="primary" className='btn-merah' onClick={()=> setSelected(ecgo3.merah)}>Ferrari Red</Button>
          <Button variant="primary" className='btn-biru' onClick={()=> setSelected(ecgo3.biru)}>Royal Blue</Button>
      </div>
      <h1>Spesifikasi Motor</h1>
      <div className='spec-container'>
          <ul>
            <li className='spec'>Power Motor      : 1000 Watt</li>
            <li className='spec'>Kecepatan Maks   : 50 Km/Jam</li>
            <li className='spec'>Jarak Tempuh     : 80 Km/Full Charge</li>
            <li className='spec'>Ukuran Ban       : 10x3 Inch</li>
            <li className='spec'>Waktu Pengecasan : 5 Jam</li>
            <li className='spec'>Dimensi          : </li>
            <li className='spec'>Kunci Utama      : Keyless Remote</li>
            <li className='spec'>Max Beban        : 150 Kg</li>
          </ul>  
          <ul>
            <li className='spec'>Rangka           : Besi</li>
            <li className='spec'>Suspensi Depan   : Teleskopik</li>
            <li className='spec'>Suspensi Belakang : Peredam Kejut Ayun Ganda</li>
            <li className='spec'>Ukuran Ban       :</li>
            <li className='spec'>Rem Depan        : Cakram Hidrolik</li>
            <li className='spec'>Rem Belakang     : Tromol Drum</li>
            <li className='spec'>Tipe Baterai     : LifePo4 48v 27Ah</li>
            <li className='spec'>Torsi : 1 Kw/ 2000rpm</li>
          </ul>
      </div>
      <div className='feature-container'>
        <Card style={{ width: '20rem', backgroundColor:'#3c3c3c', color:'white' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/Compartment.jpg')} />
          <Card.Body>
            <Card.Title>Dual Battery</Card.Title>
            <Card.Text >
            Kapasitas daya dua kali lipat menjadikan jarak tempuh menjadi semakin panjang 
            </Card.Text>
          </Card.Body>
        </Card>
        <Card style={{ width: '20rem', backgroundColor:'#3c3c3c', color:'white' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/Food box.jpg')} />
          <Card.Body>
            <Card.Title>Container Box</Card.Title>
            <Card.Text>
            Membawa barang lebih banyak menjadi lebih mudah tanpa takut terkendala cuaca
            </Card.Text>
          </Card.Body>
        </Card>
        <Card style={{ width: '20rem', backgroundColor:'#3c3c3c', color:'white' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/Speedometer.jpg')} />
          <Card.Body>
            <Card.Title>Full Panel Digital Speedometer</Card.Title>
            <Card.Text>
            Fitur indikator digital yang lebih memudahkan pengendara memahami kondisi dari kendaraan
            </Card.Text>
          </Card.Body>
        </Card>
        <Card style={{ width: '20rem', backgroundColor:'#3c3c3c', color:'white' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/charger.jpg')} />
          <Card.Body>
            <Card.Title>Fast Charging</Card.Title>
            <Card.Text>
            Hanya membutuhkan 2-3 jam untuk melakukan pengisian baterai sampai kondisi maksimal.
            </Card.Text>
          </Card.Body>
        </Card>

      </div>
      <a href={brosur}  download='brosur-ecgo-3'><Button variant='primary' style={{ width: '18rem', border:'none'}}>Download Brosur</Button></a>  
    </div>
  )
}

export default ECGO3