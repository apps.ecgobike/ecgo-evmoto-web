import {React,useState} from 'react'
import { DynamicTitle } from '../../utils/DynamicTitle'
import './ecgo5.css'
import { Button, Card } from 'react-bootstrap'
import brosur from '../../../assets/brosur5.pdf'

const ECGO5 = () => {
  DynamicTitle('ECGO 5 | ECGO')

  const putih = require('../../../assets/images/ecgo5/putih-5.png');
  const hitam = require('../../../assets/images/ecgo5/hitam-5.png');
  const merah = require('../../../assets/images/ecgo5/merah-5.png');

  const ecgo5 = {hitam, putih, merah};

  const [selected, setSelected] = useState(ecgo5.putih)

  return (
    <div className='ecgo5'>
       <div className='ecgo5-container'>
       <img id='ecgo5-img' className='ecgo5-img' src={selected}/>
      </div>
      <div className='color-container'>       
          <Button className='btn-putih' onClick={()=> setSelected(ecgo5.putih)}>Pearl White</Button>
          <Button className='btn-hitam' onClick={()=> setSelected(ecgo5.hitam)}>Iron Black</Button>
          <Button className='btn-merah' onClick={()=> setSelected(ecgo5.merah)}>Ferrari Red</Button>
      </div>
      <h1>Spesifikasi Motor</h1>
      <div className='spec-container'>
          <ul>
            <li className='spec'>Power Motor</li>
            <li className='spec'>Kecepatan Maks</li>
            <li className='spec'>Jarak Tempuh</li>
            <li className='spec'>Daya Masuk</li>
            <li className='spec'>Daya Keluar</li>
            <li className='spec'>Rangka</li>
            <li className='spec'>Rangka</li>
          </ul>
          <ul>
            <li className='spec'>Suspensi Depan</li>
            <li className='spec'>Suspensi Belakang</li>
            <li className='spec'>Ukuran Ban</li>
            <li className='spec'>Rem Depan</li>
            <li className='spec'>Rem Belakang</li>
            <li className='spec'>Tipe Baterai</li>
            <li className='spec'>Kapasitas Maks Baterai</li>
          </ul>
      </div>
      <div className='feature-container'>
        <Card style={{ width: '20rem' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/Compartment.jpg')} />
          <Card.Body>
            <Card.Title>Dual Battery</Card.Title>
            <Card.Text>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </Card.Text>
          </Card.Body>
        </Card>
        <Card style={{ width: '20rem' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/Food box.jpg')} />
          <Card.Body>
            <Card.Title>Container Box</Card.Title>
            <Card.Text>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </Card.Text>
          </Card.Body>
        </Card>
        <Card style={{ width: '20rem' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/Speedometer.jpg')} />
          <Card.Body>
            <Card.Title>Full Panel Digital Speedometer</Card.Title>
            <Card.Text>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </Card.Text>
          </Card.Body>
        </Card>
        <Card style={{ width: '20rem' }}>
          <Card.Img variant="top" src={require('../../../assets/images/ecgo3/fitur/charger.jpg')} />
          <Card.Body>
            <Card.Title>Fast Charging</Card.Title>
            <Card.Text>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </Card.Text>
          </Card.Body>
        </Card>

      </div>
      <a href={brosur} download='brosur-ecgo-5'><Button variant='primary' style={{ width: '18rem', border:'none'}}>Download Brosur</Button></a>
    </div>
  )
}

export default ECGO5