import React from 'react'
import './dealer.css'
import Card from 'react-bootstrap/Card';
import { DynamicTitle } from '../../utils/DynamicTitle';

const Dealer = () => {
  DynamicTitle('Dealer | ECGO')

  return (
    <div className='dealer'>
      <h1 className='dealer-text'>Dealer Resmi ECGO Bike</h1>
        <div className='dealer-card-container'>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Firma Jasa Layanan</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Kebayoran Baru, Jakarta Selatan</Card.Subtitle>
              <Card.Text>
                
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Bintang Electric Motor</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Mall BOGOR TRADE WORLD</Card.Subtitle>
              <Card.Text>
                Phone : +62 812-8465-2000
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Dharmawan Putra</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Grogol Petamburan</Card.Subtitle>
              <Card.Text>
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Transportasi Masa Depan</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Kebayoran Lama</Card.Subtitle>
              <Card.Text>
                Phone : +62 856-9713-8132
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Dealer Green Indo</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Bekasi Utara</Card.Subtitle>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Bisnis Sehat Harmonis</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Kec. Tebet Kota</Card.Subtitle>
              <Card.Text>
                Phone : +62 852-4554-0453
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Sejahtera Selalu</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jl. Poros Panciro, Sulawesi Selatan</Card.Subtitle>
              <Card.Text>
              +62 813-4259-7646
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Ernawati</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jl. Penggilingan Baru 1 no.99, Jaktim</Card.Subtitle>
              <Card.Text>
              +62 813-3671-6279
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Ivan Wijaya</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Yogyakarta</Card.Subtitle>
              <Card.Text>
              +62 813-1819-9999
              </Card.Text>
             
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Koperasi Konsumen Mitra</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Malang Jawa Timur</Card.Subtitle>
              <Card.Text>
              +62 822-3331-7088
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Rahmat Nur</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Sukajadi, Pekanbaru</Card.Subtitle>
              <Card.Text>
              +62 821-1221-2767
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Cahaya Maharani</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Bintan, Dumai</Card.Subtitle>
              <Card.Text>
              +62 813-6578-4389
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Indra Noviansyah</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Pontianak, Kalimantan Selatan</Card.Subtitle>
              <Card.Text>
              +62 852-4554-0453
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Karya Lestari Banua</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Kalimantan Selatan</Card.Subtitle>
              <Card.Text>
              +62 896-9201-5685
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Bayu Arum Cipto</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jatiasih Bekasi</Card.Subtitle>
              <Card.Text>
              +62 888-8982-705
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Sinergi Grafika Perkasa</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jl. H. Agus Salim No. 34, Kalimantan Barat</Card.Subtitle>
              <Card.Text>
              +62 853-8786-8880
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Yusfansyah</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Ketapang, Kalimantan Barat</Card.Subtitle>
              <Card.Text>
              +62 853-8786-8880
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Ari Wahyudi</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jl. Ibu Sangki No. 58, Cimahi Jawa Barat</Card.Subtitle>
              <Card.Text>
              +62 878-2342-8642
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Indriyaswanto</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Depok, Jawa Barat</Card.Subtitle>
              <Card.Text>
              +62 856-2990-852
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Vania Frilliandini</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jl. Cendana manis 3, no 22, Madiun</Card.Subtitle>
              <Card.Text>
              +62 822-3399-6788
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Trans Merdeka Energi</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Karangduak, Kota Sumenep </Card.Subtitle>
              <Card.Text>
              +62 858-5670-5964
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Sumber Kencana</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Surabaya, Jawa Timur</Card.Subtitle>
              <Card.Text>
              +62 813-3180-3777
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Ariadi Abimanju, ST</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Kepulauan Bangka Belitung</Card.Subtitle>
              <Card.Text>
              +62 813-8009-9993
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Yudhistira Pratama</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Tebet,Jakarta Selatan</Card.Subtitle>
              <Card.Text>
              +62 813-8999-0009
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Tiga Sahabat Berkah Abadi Motor</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">A. Yani KM 3.5, Kalimantan Tengah</Card.Subtitle>
              <Card.Text>
              +62 813-4861-2814
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Citra Mandiri</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Palu, Sulawesi Tengah</Card.Subtitle>
              <Card.Text>
              +62 811-4508-016
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Adistra Global Etrans</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Surakarta, Jawa Tengah</Card.Subtitle>
              <Card.Text>
              +62 857-8016-3745
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Depo Bangunan</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Kubu Raya, Kalimantan Barat</Card.Subtitle>
              <Card.Text>
              +62 821-5713-9432
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Mudji Widodo</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jati Uwung, Tangerang</Card.Subtitle>
              
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Muhammad Khairudin</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Ngawi, Jawa Timur</Card.Subtitle>
            
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Nasuroh</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Bekasi, Jawa Barat</Card.Subtitle>
              
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Dealer Green Indo</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Bekasi Utara </Card.Subtitle>
              
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Edwin Surjopurwanto</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Kepanjen, Kota Blitar, Jawa Timur</Card.Subtitle>
              <Card.Text>
              +62 822-1171-1171
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Bend SMK Muhammadiyah</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Rembang</Card.Subtitle>
              
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>UD. Surya Jaya Makmur</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Gresik, Jawa Timur</Card.Subtitle>
           
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Agus Sutiono</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Cengkareng</Card.Subtitle>
             
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Loyalty Development</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Penjaringan, Jakarta </Card.Subtitle>
              <Card.Text>
              +62 878-8644-8487
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Future Mulia Abadi</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Pergudangan Mutiara Kosambi</Card.Subtitle>
              <Card.Text>
              +62 813-3789-8737
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Alysa Zahra Faiqoh</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Pondok Aren, Tangerang Selatan</Card.Subtitle>
           
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Asep Iskandar</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Gunung Putri, Bogor</Card.Subtitle>
             
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Prariarga Maolana</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Batang, Jawa Tengah</Card.Subtitle>
             
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Wahyu Ginaryanto</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Madiun, Jawa Timur</Card.Subtitle>
              
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>Mahmud Yusuf</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Rappang, Sulawesi Selatan</Card.Subtitle>
           
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>CV. Makmur Jati</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Jatiuwung, Tangerang</Card.Subtitle>
              <Card.Text>
              +62 812-8225-8814
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className='dealer-card'>
            <Card.Body>
              <Card.Title>PT. Intriciti Pratama Makmur</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Ruko Madrid #3-5 Goldland Karawaci, Tangerang  </Card.Subtitle>
              <Card.Text>
              +62 813-8999-0009
              </Card.Text>
            </Card.Body>
          </Card>
        </div>
    </div>
  )
}

export default Dealer