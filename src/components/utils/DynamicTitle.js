import React from 'react'

export const DynamicTitle = (newTitle) => {
  return (
    document.title = newTitle
  )
}
